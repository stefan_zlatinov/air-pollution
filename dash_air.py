#!/usr/bin/python3
import pandas as pd
import plotly
from plotly import tools
from plotly import graph_objs as go
import plotly
import plotly.graph_objs as go
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

refresh_rate = 30 * 1000  # milliseconds
pollution_number_font_size = 200
pollution_text_font_size = 80

app = dash.Dash(__name__)

app.layout = html.Div([
    html.Div([
        html.Div([
            html.Div(children='PM 2.5', style={'fontSize': pollution_text_font_size, 'display': 'inline-block'}),
            html.Div(id='div_pm2p5', children=html.Div(), style={'display': 'inline-block'}),
            html.Div(children='μg/㎥', style={'fontSize': pollution_text_font_size, 'display': 'inline-block'}),
        ], style={'width': '50%', 'display': 'inline-block'}),
        html.Div([
            html.Div(children='PM 10', style={'fontSize': pollution_text_font_size, 'display': 'inline-block'}),
            html.Div(id='div_pm10', children=html.Div(), style={'display': 'inline-block'}),
            html.Div(children=u'μg/㎥', style={'fontSize': pollution_text_font_size, 'display': 'inline-block'}),
        ], style={'width': '50%', 'display': 'inline-block'}),
        
        dcc.Graph(id='pollution_graph'),
        dcc.Interval(id='interval-component', interval=refresh_rate, n_intervals=0)
    ])
    
])


def get_pollution_color(pollution):
    if pollution < 50:
        color = '#00ff00'  # green
    elif pollution < 100:
        color = '#ffa500'  # orange
    else:
        color = '#ff0000'  # red
    return color


@app.callback(Output('div_pm2p5', 'children'), 
              [Input('interval-component', 'n_intervals')])
def update_pm_2p5(n):
    raw_data = pd.read_csv('air_pollution_data.csv', header=None).tail()
    raw_data.columns = ['Time', 'pm_2p5', 'pm_10']
    value = int(raw_data['pm_2p5'].mean())
    color = get_pollution_color(value)
    return html.Div(value, style={'color': color, 'fontSize': pollution_number_font_size, 'textAlign': 'center'})


@app.callback(Output('div_pm10', 'children'), 
              [Input('interval-component', 'n_intervals')])
def update_pm_10(n):
    raw_data = pd.read_csv('air_pollution_data.csv', header=None).tail()
    raw_data.columns = ['Time', 'pm_2p5', 'pm_10']
    value = int(raw_data['pm_10'].mean())
    color = get_pollution_color(value)
    return html.Div(value, style={'color': color, 'fontSize': pollution_number_font_size, 'textAlign': 'center'})


@app.callback(Output('pollution_graph', 'figure'), 
              [Input('interval-component', 'n_intervals')])
def update_graph_live(n):
    raw_data = pd.read_csv('air_pollution_data.csv', header=None)
    raw_data.columns = ['Time', 'pm_2p5', 'pm_10']
    raw_data['Time'] -= raw_data['Time'].iloc[0]
    full_time = pd.Series(range(raw_data['Time'].iloc[-1] + 1))
    full_data = pd.DataFrame(full_time, columns=['Time'])
    full_data = pd.merge(full_data, raw_data, on='Time', how='left')
    full_data['Time'] -= full_data['Time'].iloc[-1] + 1
    full_data['Time'] /= 60
    trace_pm_2p5 = go.Scatter(x=full_data['Time'], y=full_data['pm_2p5'], name='PM 2.5')
    trace_pm_10 = go.Scatter(x=full_data['Time'], y=full_data['pm_10'], name='PM 10')
    traces = [trace_pm_2p5, trace_pm_10]
    title='Концентрација на PM честички во последните 6 часа'
    xaxis=dict(title='Време [min]', range=[-360, 0])
    yaxis=dict(title='Концентрација на PM честички [μg/㎥]', range=[0, 100])
    font = dict(size=18)
    layout = go.Layout(xaxis=xaxis, yaxis=yaxis, title=title, font=font)
    
    return {'data': traces, 'layout': layout}


if __name__ == '__main__':
    app.run_server()
